#include "global.h"
#include "hcms291x.h"
#include <util/delay.h>   //delay
#include <avr/io.h>

#define LED D,7

int main(void){
    int myDirection = 1;
    SET_OUTPUT(LED);
    hcms_291x_init();
    hcms_291x_set_string("This is message!");

    while (1){
// Example 1
        RESET(LED);
        hcms_291x_print("Grzegorz");
        _delay_ms(500);
        SET(LED);
        hcms_291x_print("Hetman:)");
        _delay_ms(500);

/* Example 2
        hcms_291x_clear();		
        hcms_291x_write('a');
                hcms_291x_write('b');
        hcms_291x_write('c');
        _delay_ms(500);
*/

/* Example 3
        if ((hcms_291x_get_cursor() > 8) || (hcms_291x_get_cursor() <= -(hcms_291x_string_length()))) {
            myDirection = -myDirection;
            _delay_ms(1000);
        }
        // scroll:
        hcms_291x_scroll(myDirection);
        _delay_ms(300);
*/
    }  
    return 0;
}	
