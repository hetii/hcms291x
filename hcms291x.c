#include "hcms291x.h"
#include "global.h"
#include "font5x7.h"      //Pascal Stang's 5x7 font library.
#include <avr/pgmspace.h> //The font library is stored in program memory.
#include <util/delay.h>   //delay

#include <string.h> 

uint8_t cursorPos = 0;                       // Position of the cursor in the display.

char stringBuffer[HCMS_DISPLAY_LENGTH+1]; // Default array that the displayString will point to.
//uint8_t dotRegister[40];  // The 320-bit dot register for a single 8 digit LED display.
//uint8_t dotRegister[80];  // The pair of 320-bit dot register for two 8 character LED displays.
uint8_t dotRegister[160];   // Four 320-bit dot registers. 320 for each 8 character LED display.

char* displayString;		// String for scrolling.

size_t strlen(const char *str){
    const char *s;
    for (s = str; *s; ++s);
    return(s - str);
}

// Initialize the display.
void hcms_291x_init(void) {
    
    uint8_t i;
    for (i = 0; i < HCMS_DISPLAY_LENGTH; i++) {
        stringBuffer[i] = ' ';					// Fill stringBuffer with spaces, and a trailing 0:
    }
    stringBuffer[HCMS_DISPLAY_LENGTH] = '\0';
    
    hcms_291x_set_string(stringBuffer);				// give displayString a default buffer

    // Set pin modes for connections:
    SET_OUTPUT(HCMS_DATA_IN);
    SET_OUTPUT(HCMS_RS);
    SET_OUTPUT(HCMS_CLOCK);
    SET_OUTPUT(HCMS_CE);

    // Active SPI master interface.
#if HCMS_USE_HARDWARE_SPI == 1
    //SPCR = (1<<SPE)|(1<<MSTR) | (0<<SPR1)|(1<<SPR0);
    SPCR = (1<<SPE)|(1<<MSTR) | (0<<SPR1)|(0<<SPR0);
    SPSR = 0;
#endif

#ifdef HCMS_RESET
    SET_OUTPUT(HCMS_RESET);
    // Reset the display:
    RESET(HCMS_RESET);
    _delay_ms(10);
    SET(HCMS_RESET);
#endif

    // Load dot register with lows.
    hcms_291x_load_dot_register();

    hcms_291x_load_control_register(0b10000001); // set serial mode. see table 1, footnote 1
    // set control register 0 for max brightness, and no sleep:
    // added: ML send multiple inits to 2nd, 3rd, 4th display, etc.
    // set control register 0 for max brightness, and no sleep:
    hcms_291x_load_control_register(0b01111111);
    hcms_291x_load_control_register(0b01111111);
    hcms_291x_load_control_register(0b01111111);
    hcms_291x_load_control_register(0b01111111);
    hcms_291x_load_control_register(0b01111111);
    hcms_291x_load_control_register(0b01111111);
    hcms_291x_load_control_register(0b01111111);
    // set control register 1 so all 8 characters display:
    // hcms_291x_load_control_register(B10000001); 

    // Clear display and set default brightness to 7
    hcms_291x_clear();
    hcms_291x_set_brightness(7);
}

// Clear the display.
void hcms_291x_clear(void) {
    
    uint8_t displayPos;
    char charToShow;
    //TODO
    // Needed if use hcms_291x_write() to clear position.
    //hcms_291x_home();

    for (displayPos = 0; displayPos < HCMS_DISPLAY_LENGTH; displayPos++) {
        charToShow = ' ';
        // put the character in the dot register:
        hcms_291x_write_character(charToShow, displayPos);  
    }
    // send the dot register array out to the display:
    hcms_291x_load_dot_register();
}

// Set the cursor to the home position (0)
void hcms_291x_home(void) {
    // set the cursor to the upper left corner:
    cursorPos = 0;
}

// Set the cursor anywhere.
void hcms_291x_set_cursor(uint8_t whichPosition) {
    cursorPos = whichPosition;
}

// Return the cursor position.
uint8_t hcms_291x_get_cursor(void) {
    return cursorPos;
}

// Write a byte out to the display at the cursor position,
// and advance the cursor position.
void hcms_291x_write(uint8_t b) {
    // Make sure cursorPos is on the display:
    if (cursorPos >= 0 && cursorPos < HCMS_DISPLAY_LENGTH) {
        // Put the character into the dot register:
        hcms_291x_write_character(b, cursorPos);
        // Put the character into the displayString:
        if (cursorPos < hcms_291x_string_length()) {
            displayString[cursorPos] = b;
        }		
        cursorPos++;
        // Send the dot register array out to the display:
        hcms_291x_load_dot_register();
    }
}

// Scroll the displayString across the display.  left = -1, right = +1
void hcms_291x_scroll(int direction) {
    uint8_t displayPos, whichCharacter, stringEnd;
    char charToShow; 

    hcms_291x_clear();
    cursorPos += direction;
    // Loop over the string and take HCMS_DISPLAY_LENGTH characters to write to the display:
    for (displayPos = 0; displayPos < HCMS_DISPLAY_LENGTH; displayPos++) {
        // which character in the strings you want:
        whichCharacter = displayPos - cursorPos;
        //  length of the string to display:
        stringEnd = strlen(displayString);
        // which character you want to show from the string:
      
        // display the characters until you have no more:
        if ((whichCharacter >= 0) && (whichCharacter < stringEnd)) {
            charToShow = displayString[whichCharacter]; 
            // if none of the above, show a space:
        } else {
            charToShow = ' ';
        }
        // put the character in the dot register:
        hcms_291x_write_character(charToShow, displayPos);  
    }
    // send the dot register array out to the display:
    hcms_291x_load_dot_register();
}

// Set displayString
void hcms_291x_set_string(char* _displayString) {
    displayString = _displayString;
}

// Return displayString.
char* hcms_291x_get_string(void) {
    return displayString;
}

// Return displayString length.
int hcms_291x_string_length(void) {
    return strlen(displayString);
}	
    
// Set brightness (0 - 15).
void hcms_291x_set_brightness(uint8_t bright) {
    // Set the brightness:
    hcms_291x_load_control_register(0b01110000 + bright);    
}

// This method loads bits into the dot register array.
// It doesn't actually communicate with the display at all,
// it just prepares the data:
void hcms_291x_write_character(char whatCharacter, uint8_t whatPosition) {
    // Calculate the starting position in the array.
    // Every character has 5 columns made of 8 bits:
    uint8_t i;
    uint8_t thisPosition =  whatPosition * 5;

    // copy the appropriate bits into the dot register array:
    for (i = 0; i < 5; i++) {
        dotRegister[thisPosition+i] = (pgm_read_byte(&Font5x7[((whatCharacter - 0x20) * 5) + i]));
    }
}

// This method sends 8 bits to one of the control registers:
void hcms_291x_load_control_register(uint8_t dataByte){
    // Select the control registers:
    SET(HCMS_RS);
    // Enable writing to the display:
    RESET(HCMS_CE);
    // Shift the data out:
    // shift_out(HCMS_DATA_IN, HCMS_CLOCK, MSBFIRST, dataByte);
    hcms_291x_shift_out(dataByte);
    // Disable writing:
    SET(HCMS_CE);
}

// This method originally sent 320 bits to the dot register: 12_30_09 ML
void hcms_291x_load_dot_register(void) {
    // define max data to send, patch for 4 length displays by KaR]V[aN
    uint8_t maxData = HCMS_DISPLAY_LENGTH * 5;
    uint8_t i;
    // select the dot register:
    RESET(HCMS_RS);
    // Enable writing to the display:
    RESET(HCMS_CE);
    // Shift the data out:
    for (i = 0; i < maxData; i++) {
        //shift_out(HCMS_DATA_IN, HCMS_CLOCK, MSBFIRST, dotRegister[i]);
        hcms_291x_shift_out(dotRegister[i]);
    }
    // Disable writing:
    SET(HCMS_CE);
}

void hcms_291x_shift_out(uint8_t val) {
#if HCMS_USE_HARDWARE_SPI == 1
    // put byte in send-buffer
    SPDR = val;
    // wait until byte was send
    while(!(SPSR & (1<<SPIF)));
#else
    uint8_t i;
    for (i = 0; i < 8; i++) {
        //digitalWrite(dataPin, !!(val & (1 << (7 - i))));
        if (!!(val & (1 << (7 - i)))){
            SET(HCMS_DATA_IN);
        }else{
            RESET(HCMS_DATA_IN);
        }
        SET(HCMS_CLOCK);
        RESET(HCMS_CLOCK);
    }
#endif
}

void hcms_291x_print(char* text) {
    hcms_291x_home();
    hcms_291x_clear();
    while(*text){
        hcms_291x_write(*text++);
    }
}
